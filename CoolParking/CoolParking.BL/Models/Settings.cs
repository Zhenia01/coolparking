﻿using System.Collections.Generic;
using System.Collections.Immutable;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public const decimal StartBalance = 0;
        public const int Capacity = 10;
        public const int WithdrawPeriod = 5;
        public const int LogPeriod = 60;
        public const decimal FineRatio = 2.5m;

        public static readonly ImmutableDictionary<VehicleType, decimal> VehicleTypeRates =
            new Dictionary<VehicleType, decimal>
            {
                [VehicleType.Bus] = 3.5m,
                [VehicleType.PassengerCar] = 2m,
                [VehicleType.Truck] = 5m,
                [VehicleType.Motorcycle] = 1m,
            }.ToImmutableDictionary();
    }
}