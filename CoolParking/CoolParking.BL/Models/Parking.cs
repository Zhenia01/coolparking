﻿using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public sealed class Parking
    {
        private Parking()
        {
        }

        public static Parking Instance { get; } = new Parking();

        public decimal Balance { get; internal set; } = Settings.StartBalance;

        public List<Vehicle> Vehicles { get; } = new List<Vehicle>();

        public void Clear()
        {
            Vehicles.Clear();
            Balance = Settings.StartBalance;
        }
    }
}