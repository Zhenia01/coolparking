﻿using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public decimal Sum { get; }

        public DateTime Time { get; }

        public string Id { get; }

        public TransactionInfo(Vehicle vehicle, DateTime time, decimal sum)
        {
            Id = vehicle.Id;
            Time = time;
            Sum = sum;
        }
    }
}