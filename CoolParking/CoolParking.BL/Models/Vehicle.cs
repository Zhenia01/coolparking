﻿using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }

        private static readonly Regex IdRegex = new Regex(@"[A-Z]{2}-[0-9]{4}-[A-Z]{2}");

        public static bool IsIdValid(string id)
        {
            try
            {
                return IdRegex.IsMatch(id);
            }
            catch (ArgumentNullException)
            {
                return false;
            }
        }

        public Vehicle(string id, VehicleType type, decimal balance)
        {
            if (!IsIdValid(id)) throw new ArgumentException("Invalid Id pattern");
            if (balance <= 0) throw new ArgumentException("Balance can't be non-positive");
            Id = id;
            Balance = balance;
            VehicleType = type;
        }
        
        private static readonly Random Random = new Random();

        public static string GenerateRandomRegistrationPlateNumber()
        {
            static char RandomUppercaseLetter()
            {
                return (char) Random.Next('A', 'Z');
            }

            static char RandomDigit()
            {
                return (char) Random.Next('0', '9');
            }

            return
                $"{RandomUppercaseLetter()}{RandomUppercaseLetter()}-{RandomDigit()}{RandomDigit()}{RandomDigit()}{RandomDigit()}-{RandomUppercaseLetter()}{RandomUppercaseLetter()}";
        }
    }
}