﻿using System.Timers;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private readonly Timer _timer = new Timer();

        public event ElapsedEventHandler Elapsed;
        public double Interval { get; set; }

        public void Start()
        {
            _timer.Elapsed += Elapsed;
            _timer.Interval = Interval * 1000;
            _timer.Start();
        }

        public void Stop()
        {
            _timer.Stop();
        }

        #region Dispose Pattern

        private bool _disposed;

        public void Dispose()
        {
            if (!_disposed)
            {
                _timer.Close();
                _disposed = true;
            }
        }

        #endregion
    }
}