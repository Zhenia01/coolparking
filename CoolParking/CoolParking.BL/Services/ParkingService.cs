﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public sealed class ParkingService : IParkingService
    {
        private static readonly Parking Parking = Parking.Instance;

        private readonly ITimerService _withdrawTimer;
        private readonly ITimerService _logTimer;
        private readonly ILogService _logService;

        private readonly List<TransactionInfo> _transactions = new List<TransactionInfo>();

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _logService = logService;

            _logTimer = logTimer;
            _logTimer.Interval = Settings.LogPeriod;
            _logTimer.Elapsed += Log;

            _withdrawTimer = withdrawTimer;
            _withdrawTimer.Interval = Settings.WithdrawPeriod;
            _withdrawTimer.Elapsed += Withdraw;

            _logTimer.Start();
            _withdrawTimer.Start();
        }

        private void Withdraw(object o, EventArgs args)
        {
            foreach (var vehicle in Parking.Vehicles)
            {
                var rate = Settings.VehicleTypeRates[vehicle.VehicleType];
                decimal withdraw = rate;

                if (vehicle.Balance < rate)
                {
                    withdraw += -((vehicle.Balance - rate) * Settings.FineRatio);
                }

                vehicle.Balance -= withdraw;
                Parking.Balance += withdraw;

                _transactions.Add(new TransactionInfo(vehicle, DateTime.Now, withdraw));
            }
        }

        private void Log(object o, EventArgs args)
        {
            StringBuilder log = new StringBuilder();
            foreach (TransactionInfo transaction in _transactions)
            {
                log.AppendLine($"[{transaction.Time:dd/MM/yyyy HH:mm:ss}] {transaction.Id}: {transaction.Sum}");
            }

            _logService.Write(log.ToString());
            _transactions.Clear();
        }

        public decimal GetBalance()
        {
            return Parking.Balance;
        }

        public int GetCapacity()
        {
            return Settings.Capacity;
        }

        public int GetFreePlaces()
        {
            return GetCapacity() - GetVehicles().Count;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return Parking.Vehicles.AsReadOnly();
        }

        private static Vehicle GetVehicleById(string id)
        {
            return Parking.Vehicles.Find(v => v.Id == id);
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (GetVehicleById(vehicle.Id) != null) throw new ArgumentException();
            if (GetFreePlaces() == 0) throw new InvalidOperationException("The parking is full");

            Parking.Vehicles.Add(vehicle);
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicle = GetVehicleById(vehicleId);
            if (vehicle == null) throw new ArgumentException();
            Parking.Vehicles.Remove(vehicle);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum < 0) throw new ArgumentException("Top up sum can't be non-positive");
            var vehicle = GetVehicleById(vehicleId);
            if (vehicle != null)
            {
                vehicle.Balance += sum;
            }
            else
            {
                throw new ArgumentException($"Vehicle with id {vehicleId} doesn't exist");
            }
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _transactions.ToArray();
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }


        #region Dispose Pattern

        private bool _disposed;

        public void Dispose()
        {
            if (!_disposed)
            {
                Parking.Clear();
                _logTimer.Dispose();
                _withdrawTimer.Dispose();
                _disposed = true;
            }
        }

        #endregion
    }
}