﻿using System;
using System.IO;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; }

        public LogService(string logPath)
        {
            LogPath = logPath;
        }

        public void Write(string logInfo)
        {
            using StreamWriter writer = new StreamWriter(LogPath, true);
            writer.WriteLine(logInfo);
        }

        public string Read()
        {
            try
            {
                using StreamReader reader = new StreamReader(LogPath);
                return reader.ReadToEnd();
            }
            catch (FileNotFoundException e)
            {
                throw new InvalidOperationException("The file doesn't exist", e);
            }
        }
    }
}