using System;
using System.Threading.Tasks;
using CoolParking.I.Interfaces;
using CoolParking.I.Models;

namespace CoolParking.I
{
    public class Application
    {
        private readonly ICommandService _commandService;

        public Application(ICommandService commandService)
        {
            _commandService = commandService;
        }

        public async Task Start()
        {
            OpenMenu();
            Command command = Command.None;
            while (command != Command.Exit)
            {
                Console.WriteLine("Type a command: ");
                string input = Console.ReadLine();

                command = _commandService.GetCommandFromInput(input);

                while (command == Command.None)
                {
                    Console.WriteLine("Invalid command. Try again. Type a command: ");
                    input = Console.ReadLine();
                    command = _commandService.GetCommandFromInput(input);
                }

                await _commandService.ExecuteCommand(command);
            }
        }

        private static void OpenMenu()
        {
            Console.WriteLine("Welcome to Parking Application");
            Console.WriteLine("\t== List of commands ==");
            Console.WriteLine(
                "balance\t\t\t Display current parking balance\n" +
                "profit\t\t\t Display earned profit since last log\n" +
                "places\t\t\t Display count of free places\n" +
                "transactions current\t Display transactions since last log\n" +
                "transactions all\t Display all transactions\n" +
                "vehicles list\t\t Display vehicles\n" +
                "vehicles add\t\t Place a vehicle\n" +
                "vehicles remove\t\t Remove a vehicle\n" +
                "vehicles topup\t\t Top up a vehicle\n" +
                "exit\t\t\t Exit\n\n");
        }
    }
}