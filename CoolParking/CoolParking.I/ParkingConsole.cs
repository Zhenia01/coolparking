﻿using System.Threading.Tasks;
using CoolParking.I.Services;

namespace CoolParking.I
{
    class ParkingConsole
    {
        static async Task Main(string[] args)
        {
            Application app = new Application(new CommandService());
            await app.Start();
        }
    }
}