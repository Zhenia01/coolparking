using System.Threading.Tasks;
using CoolParking.I.Models;

namespace CoolParking.I.Interfaces
{
    public interface ICommandService
    {
        Command GetCommandFromInput(string input);
        Task ExecuteCommand(Command command);
    }
}