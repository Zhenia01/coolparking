namespace CoolParking.I.Models
{
    public enum Command
    {
        None,
        Balance,
        Profit,
        Places,
        TransactionsCurrent,
        TransactionsAll,
        VehiclesList,
        VehiclesAdd,
        VehiclesRemove,
        VehiclesTopUp,
        Exit
    }
}