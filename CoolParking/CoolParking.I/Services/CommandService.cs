using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using CoolParking.BL.Models;
using CoolParking.I.Interfaces;
using CoolParking.I.Models;
using CoolParking.WebAPI.Models;
using System.Net.Http.Json;
using System.Text.Json;
using CoolParking.WebAPI.Models.Dto;

namespace CoolParking.I.Services
{
    public class CommandService : ICommandService
    {
        private static readonly HttpClient HttpClient;

        static CommandService()
        {
            HttpClient = new HttpClient();
        }

        public Command GetCommandFromInput(string input)
        {
            input = input?.Replace(" ", string.Empty);

            Enum.TryParse(input, true, out Command command);

            return command;
        }

        public async Task ExecuteCommand(Command command)
        {
            switch (command)
            {
                case Command.None:
                    break;
                case Command.Balance:
                    await BalanceCommand();
                    break;
                case Command.Profit:
                    await ProfitCommand();
                    break;
                case Command.Places:
                    await PlacesCommand();
                    break;
                case Command.TransactionsCurrent:
                    await TransactionsCurrentCommand();
                    break;
                case Command.TransactionsAll:
                    await TransactionsAllCommand();
                    break;
                case Command.VehiclesList:
                    await VehiclesListCommand();
                    break;
                case Command.VehiclesAdd:
                    await VehiclesAddCommand();
                    break;
                case Command.VehiclesRemove:
                    await VehiclesRemoveCommand();
                    break;
                case Command.VehiclesTopUp:
                    await VehiclesTopUpCommand();
                    break;
                case Command.Exit:
                    ExitCommand();
                    break;
                default:
                    ExitCommand();
                    break;
            }
        }

        private static async Task TransactionsAllCommand()
        {
            try
            {
                using HttpResponseMessage response =
                    await HttpClient.GetAsync(ApiEndpoints.Transactions.GetAllTransactions);
                if (response.StatusCode == HttpStatusCode.NotFound)
                {
                    Console.WriteLine("Log file is not found");
                }
                else
                {
                    Console.WriteLine(await response.Content.ReadAsStringAsync());
                }
            }
            catch (HttpRequestException)
            {
                Console.WriteLine("A server error has occured");
            }
        }

        private static void ExitCommand()
        {
            Console.WriteLine("Goodbye");
        }

        private static async Task VehiclesTopUpCommand()
        {
            string id = ReadId();
            var body = new TopUpVehicleDto {Id = id, Sum = ReadBalance()};
            try
            {
                using HttpResponseMessage response =
                    await HttpClient.PutAsJsonAsync(ApiEndpoints.Transactions.PutTopUpVehicle, body);
                switch (response.StatusCode)
                {
                    case HttpStatusCode.BadRequest:
                        Console.WriteLine("Invalid parameters");
                        break;
                    case HttpStatusCode.NotFound:
                        Console.WriteLine("Vehicle is not found");
                        break;
                }
            }
            catch (HttpRequestException)
            {
                Console.WriteLine("A server error has occured");
            }
        }

        private static string ReadId()
        {
            Console.WriteLine("Id: ");
            string input = Console.ReadLine();
            while (!Vehicle.IsIdValid(input))
            {
                Console.WriteLine("Invalid id pattern. Try again.\nId:");
                input = Console.ReadLine();
            }

            return input;
        }

        private static decimal ReadBalance()
        {
            Console.Write("Balance: ");
            decimal balance;
            while (!decimal.TryParse(Console.ReadLine(), out balance) || balance <= 0)
            {
                Console.Write("Balance should be a positive number. Try again. Balance:");
            }

            return balance;
        }

        private static async Task VehiclesRemoveCommand()
        {
            string id = ReadId();
            try
            {
                using HttpResponseMessage response =
                    await HttpClient.DeleteAsync(ApiEndpoints.Vehicles.DeleteVehicle(id));
                switch (response.StatusCode)
                {
                    case HttpStatusCode.BadRequest:
                        Console.WriteLine("Id is invalid");
                        break;
                    case HttpStatusCode.NotFound:
                        Console.WriteLine("Vehicle is not found");
                        break;
                }
            }
            catch (HttpRequestException)
            {
                Console.WriteLine("An error has occured");
            }
        }

        private static async Task VehiclesAddCommand()
        {
            try
            {
                using HttpResponseMessage freeSpaceResponse =
                    await HttpClient.GetAsync(ApiEndpoints.Parking.GetFreePlaces);
                if (!int.TryParse(await freeSpaceResponse.Content.ReadAsStringAsync(), out int a) || a == 0)
                {
                    Console.WriteLine("An error occured");
                }
                else
                {
                    Console.WriteLine("Type the parameters:");

                    string id = ReadId();

                    Console.WriteLine(
                        "Type - provide a number:\n" +
                        "1 - Passenger Car\n" +
                        "2 - Truck\n" +
                        "3 - Bus\n" +
                        "4 - Motorcycle");
                    VehicleType? type = null;
                    while (type == null)
                    {
                        int.TryParse(Console.ReadLine(), out var i);

                        type = i switch
                        {
                            1 => VehicleType.PassengerCar,
                            2 => VehicleType.Truck,
                            3 => VehicleType.Bus,
                            4 => VehicleType.Motorcycle,
                            _ => null
                        };
                    }

                    decimal balance = ReadBalance();

                    VehicleDto vehicle = new VehicleDto {Balance = balance, Id = id, VehicleType = type.Value};

                    using HttpResponseMessage addVehicleResponse =
                        await HttpClient.PostAsJsonAsync(ApiEndpoints.Vehicles.PostVehicle, vehicle);

                    if (addVehicleResponse.StatusCode == HttpStatusCode.BadRequest)
                    {
                        Console.WriteLine("Invalid parameters");
                    }
                }
            }
            catch (HttpRequestException)
            {
                Console.WriteLine("A server error has occured");
            }
        }

        private static async Task VehiclesListCommand()
        {
            try
            {
                var vehicles =
                    (await HttpClient.GetFromJsonAsync<IEnumerable<VehicleDto>>(ApiEndpoints.Vehicles.GetVehicles))
                    .ToImmutableList();

                if (vehicles.Any())
                {
                    foreach (VehicleDto vehicle in vehicles)
                    {
                        Console.WriteLine($"{vehicle.VehicleType} {vehicle.Id} : {vehicle.Balance}");
                    }
                }
                else
                {
                    Console.WriteLine("Parking is empty");
                }
            }
            catch (HttpRequestException)
            {
                Console.WriteLine("A server error has occured");
            }
            catch (JsonException)
            {
                Console.WriteLine("Invalid parameters");
            }
        }

        private static async Task<ImmutableList<TransactionInfoDto>> CurrentTransactions()
        {
            var transactions =
                (await HttpClient.GetFromJsonAsync<IEnumerable<TransactionInfoDto>>(ApiEndpoints.Transactions
                    .GetLastTransactions)).ToImmutableList();

            return transactions;
        }

        private static async Task TransactionsCurrentCommand()
        {
            try
            {
                var transactions = await CurrentTransactions();
                if (!transactions.Any())
                {
                    Console.WriteLine("List of current transactions is empty.");
                }
                else
                {
                    foreach (TransactionInfoDto transaction in transactions)
                    {
                        Console.WriteLine($"{transaction.Id}: {transaction.Sum} {transaction.Time}");
                    }
                }
            }
            catch (HttpRequestException)
            {
                Console.WriteLine("A server error has occured");
            }
            catch (JsonException)
            {
                Console.WriteLine("Invalid parameters");
            }
        }

        private static async Task PlacesCommand()
        {
            try
            {
                using HttpResponseMessage freePlacesResponse =
                    await HttpClient.GetAsync(ApiEndpoints.Parking.GetFreePlaces);
                using HttpResponseMessage capacityResponse =
                    await HttpClient.GetAsync(ApiEndpoints.Parking.GetCapacity);

                string freePlaces = await freePlacesResponse.Content.ReadAsStringAsync();
                string capacity = await capacityResponse.Content.ReadAsStringAsync();

                Console.WriteLine(
                    $"Count of free places: {freePlaces}/{capacity}");
            }
            catch (HttpRequestException)
            {
                Console.WriteLine("A server error has occured");
            }
        }

        private static async Task BalanceCommand()
        {
            using HttpResponseMessage response = await HttpClient.GetAsync(ApiEndpoints.Parking.GetBalance);
            Console.WriteLine($"Parking balance: {await response.Content.ReadAsStringAsync()}");
        }

        private static async Task ProfitCommand()
        {
            try
            {
                var transactions = await CurrentTransactions();
                Console.WriteLine($"Parking last profit: {transactions.Sum(t => t.Sum)}");
            }
            catch (HttpRequestException)
            {
                Console.WriteLine("A server error has occured");
            }
            catch (JsonException)
            {
                Console.WriteLine("Invalid parameters");
            }
        }
    }
}