namespace CoolParking.WebAPI.Models
{
    public static class ApiEndpoints
    {
        private const string BaseUrl = "https://localhost:44313/api";

        public static class Parking
        {
            private const string Category = "parking";

            private static readonly string Route = $"{BaseUrl}/{Category}";

            public static readonly string GetBalance = $"{Route}/balance";
            public static readonly string GetCapacity = $"{Route}/capacity";
            public static readonly string GetFreePlaces = $"{Route}/freePlaces";
        }

        public static class Vehicles
        {
            private const string Category = "vehicles";

            private static readonly string Route = $"{BaseUrl}/{Category}";

            public static readonly string GetVehicles = $"{Route}";
            public static string GetVehicle(string id) => $"{Route}/{id}";

            public static readonly string PostVehicle = $"{Route}";

            public static string DeleteVehicle(string id) => $"{Route}/{id}";
        }

        public static class Transactions
        {
            private const string Category = "transactions";

            private static readonly string Route = $"{BaseUrl}/{Category}";

            public static readonly string GetLastTransactions = $"{Route}/last";
            public static readonly string GetAllTransactions = $"{Route}/all";

            public static readonly string PutTopUpVehicle = $"{Route}/topUpVehicle";
        }
    }
}