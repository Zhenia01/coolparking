using CoolParking.BL.Models;

namespace CoolParking.WebAPI.Models.Dto
{
    public class VehicleDto
    {
        public string Id { get; set; }
        public VehicleType VehicleType { get; set; }
        public decimal Balance { get;  set; }
    }
}