using System;

namespace CoolParking.WebAPI.Models.Dto
{
    public class TransactionInfoDto
    {
        public decimal Sum { get; set; }

        public DateTime Time { get; set; }

        public string Id { get; set; }
    }
}