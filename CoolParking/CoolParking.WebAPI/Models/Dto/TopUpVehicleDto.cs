namespace CoolParking.WebAPI.Models.Dto
{
    public class TopUpVehicleDto
    {
        public string Id { get; set; }
        public decimal Sum { get; set; }
    }
}