using System;
using System.Collections.Generic;
using System.Linq;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models.Dto;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public VehiclesController(IParkingService service)
        {
            _parkingService = service;
        }

        [HttpGet]
        public IEnumerable<VehicleDto> GetVehicles()
        {
            return _parkingService.GetVehicles().Select(v => new VehicleDto
            {
                Balance = v.Balance,
                Id = v.Id,
                VehicleType = v.VehicleType
            });
        }

        [HttpGet("{id}")]
        public ActionResult<VehicleDto> GetVehicle(string id)
        {
            if (!Vehicle.IsIdValid(id)) return BadRequest("Invalid ID pattern");

            var vehicles = _parkingService.GetVehicles();

            try
            {
                var vehicle = vehicles.First(v => v.Id == id);
                return new VehicleDto
                {
                    VehicleType = vehicle.VehicleType,
                    Id = vehicle.Id,
                    Balance = vehicle.Balance
                };
            }
            catch (InvalidOperationException)
            {
                return NotFound($"Vehicle with id {id} does not exist");
            }
        }

        [HttpPost]
        public ActionResult<VehicleDto> CreateVehicle([FromBody] VehicleDto vehicleDto)
        {
            if (!Enum.IsDefined(typeof(VehicleType), vehicleDto.VehicleType)) return BadRequest("Invalid vehicle type");

            try
            {
                var vehicle = new Vehicle(vehicleDto.Id, vehicleDto.VehicleType, vehicleDto.Balance);
                _parkingService.AddVehicle(vehicle);
            }
            catch (ArgumentException)
            {
                return BadRequest("Invalid parameters");
            }
            catch (InvalidOperationException)
            {
                return BadRequest("No free space available");
            }

            return Created(new Uri($"{Request.Path}", UriKind.Relative), vehicleDto);
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteVehicle(string id)
        {
            if (!Vehicle.IsIdValid(id)) return BadRequest("Invalid ID pattern");

            try
            {
                _parkingService.RemoveVehicle(id);
            }
            catch (ArgumentException)
            {
                return NotFound($"Vehicle with id {id} does not exist");
            }

            return NoContent();
        }
    }
}