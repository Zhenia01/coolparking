using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ParkingController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public ParkingController(IParkingService service)
        {
            _parkingService = service;
        }

        [HttpGet("balance")]
        public decimal GetBalance()
        {
            return _parkingService.GetBalance();
        }

        [HttpGet("capacity")]
        public int GetCapacity()
        {
            return _parkingService.GetCapacity();
        }

        [HttpGet("freePlaces")]
        public int GetFreePlaces()
        {
            return _parkingService.GetFreePlaces();
        }
    }
}