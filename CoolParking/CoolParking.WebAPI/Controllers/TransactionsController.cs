using System;
using System.Collections.Generic;
using System.Linq;
using CoolParking.BL.Interfaces;
using CoolParking.WebAPI.Models.Dto;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService _parkingService;
        private readonly ILogService _logService;

        public TransactionsController(IParkingService parkingService, ILogService logService)
        {
            _parkingService = parkingService;
            _logService = logService;
        }

        [HttpGet("last")]
        public IEnumerable<TransactionInfoDto> GetLastTransactions()
        {
            return _parkingService.GetLastParkingTransactions().Select(t => new TransactionInfoDto
            {
                Id = t.Id,
                Sum = t.Sum,
                Time = t.Time
            });
        }

        [HttpGet("all")]
        public ActionResult<string> GetAllTransactions()
        {
            try
            {
                return _logService.Read();
            }
            catch (InvalidOperationException)
            {
                return NotFound("Transactions file does not exist");
            }
        }

        [HttpPut("topUpVehicle")]
        public ActionResult<VehicleDto> TopUpVehicle([FromBody] TopUpVehicleDto topUpVehicleDto)
        {
            if (topUpVehicleDto.Id == null || topUpVehicleDto.Sum <= 0) return BadRequest("Invalid parameters");
            try
            {
                _parkingService.TopUpVehicle(topUpVehicleDto.Id, topUpVehicleDto.Sum);
            }
            catch (ArgumentException)
            {
                return NotFound($"Vehicle with id {topUpVehicleDto.Id} does not exist");
            }

            var vehicle = _parkingService.GetVehicles().First(v => v.Id == topUpVehicleDto.Id);

            return new VehicleDto
            {
                Id = vehicle.Id,
                VehicleType = vehicle.VehicleType,
                Balance = vehicle.Balance
            };
        }
    }
}